Use Case 1: Wykonanie przelewu
=====================

**Aktor podstawowy:** Klient


Główni odbiorcy i oczekiwania względem systemu:
-----------------------------------------------

- Klient: Oczekuje szybkiego wykonania przelewu.

- Klient: Oczekuje potwierdzenia udanego/nie udanego przelewu. 

- Bank: Oczekuje stałej łączności z bankomatem.

Warunki wstępne:
----------------

- Klient jest zautoryzowany (wykonał "Use case 1")
- Klient musi mieć nadane w banku uprawnienia do wykonywania przelewów za pomocą bankomatu.

Warunki końcowe:
----------------

- Pieniądze o zadanej kwocie trafiają na zadane konto.

Scenariusz główny (ścieżka podstawowa):
---------------------------------------

  1. Klient wybiera opcję wykonania przelewu.
  2. Bankomat wyświetla pytanie o podanie numeru konta odbiorcy.
  3. Klient wprowadza numer konta odbiorcy używając klawiatury numerycznej i zatwierdza zielonym przyciskiem lub wybierając odpowiednią opcję na ekranie dotykowym.
  4. Bankomat zatwierdza wprowadzone dane.
  5. Bankomat wyświetla pytanie o podanie kwoty przelewu.
  6. Klient wprowadza kwotę przelewu używając klawiatury numerycznej i zatwierdza zielonym przyciskiem lub wybierając odpowiednią opcję na ekranie dotykowym.
  7. Bankomat zatwierdza wprowadzone dane.
  8. Bankomat wyświetla pytanie o podanie tytułu przelewu.
  9. Klient wprowadza tytuł przelewu używając klawiatury numerycznej T9 i zatwierdza zielonym przyciskiem lub wybierając odpowiednią opcję na ekranie dotykowym.
  10. Bankomat wyświetla wprowadzone informacje (numer konta odbiorcy, kwota przelewu, tytuł przelewu) przy każdej opcji występuje przycisk edycji.
  11. Klient zatwierdza poprawność wprowadzonych informacji wybierając opcję na ekranie dotykowym dotyczącą wysłania przelewu.
  12. Bankomat wykonuje transakcje.
  13. Bankomat wydaje kartę i drukuje potwierdzenie przelewu.
  14. Klient zabiera kartę i potwierdzenie.
  

  
Rozszerzenia (ścieżki alternatywne):
------------------------------------

 1-12a. Czerwony przycisk na klawiaturze numerycznej anuluje dotychczas wprowadzone kroki i oddaje kartę.

 4a. Klient wprowadził nieistniejący numer konta:
	 1. Bankomat przechodzi do kroku 2 scenariusza głównego i wyświetla w nagłówku komunikat o błędzie.

 7a. Klient wprowadził kwotę wyższą niż posiada:
	 1. Bankomat przechodzi do kroku 5 scenariusza głównego i wyświetla w nagłówku komunikat o błędzie.

 11a. Klient chciałby zedytować informację o numerze konta odbiorcy:
	 1. Klient wybiera opcję edycji przy numerze konta.
	 2. Bankomat wykonuje kroki 2-4 scenariusza głównego edytując dane.
	 3. Bankomat przechodzi do punktu 10.
	 
 11b. Klient chciałby zedytować informację o kwocie przelewu:
	 1. Klient wybiera opcję edycji przy kwocie przelewu.
	 2. Bankomat wykonuje kroki 5-7 scenariusza głównego edytując dane.
	 3. Bankomat przechodzi do kroku 10 scenariusza głównego.
	 
 11c. Klient chciałby zedytować informację o tytule przelewu:
	 1. Klient wybiera opcję edycji przy tytule przelewu.
	 2. Bankomat wykonuje kroki 8-9 scenariusza głównego edytując dane.
	 3. Bankomat przechodzi do kroku 10 scenariusza głównego.

Wymagania specjalne:
--------------------

  - Klawiatura numeryczna T9 z dodatkowymi przyciskami w kolorze czerwonym, żółtym i zielonym.

Wymagania technologiczne oraz ograniczenia na wprowadzane dane:
---------------------------------------------------------------

 *a. Ekran dotykowy

 1a. Klient wybiera opcje na ekranie dotykowym.

 3,6,9a. Klient wprowadza i zatwierdza wprowadzone informacje za pomocą klawiatury numerycznej T9.

Kwestie otwarte:
----------------

  - ...