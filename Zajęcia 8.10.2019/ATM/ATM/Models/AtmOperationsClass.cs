﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM.Models
{
    class AtmOperationsClass
    {
        static public double GetAccountBalance(string Session, account userDB)
        {
            return AccountOperationsClass.GetAccountBalance(Session, userDB);
        }

        static public bool Withdrow(string Session, account userDB, DataClassesBankDataContext bankDB, DataClassesATMDataContext atmDB)
        {
            bool ok;
            while (true)
            {
                Console.WriteLine("Jaką kwotę chcesz wypłacić?");
                ok = int.TryParse(Console.ReadLine(),out int money);

                if (!ok || money < 0 )
                {
                    Console.WriteLine();
                    Console.WriteLine("Podałeś niedozwolony znak! Podaj kwotę całkowitą!");
                    continue;
                }

                if (!EnoughMoneyATM(atmDB, money))
                {
                    Console.WriteLine();
                    Console.WriteLine("Podaj kwotę, którą można uzyskać z wieloktrotności 20 lub/i 50!");
                    continue;
                }

                if (GetAccountBalance(Session, userDB) < money)
                {
                    Console.WriteLine();
                    Console.WriteLine("Brak odpowiedniej ilości pieniędzy na koncie!");
                    continue;
                }

                if (!AccountOperationsClass.DebitAccount(Session, userDB, bankDB, money))
                {
                    Console.WriteLine();
                    Console.WriteLine("Błąd! Środki nie zostały pobrane.");
                    return false;
                }


                break;

            }

            Console.WriteLine("~Przygotowywanie pieniędzy~");

            return true;
        }

        static public bool Ante(string Session, account userDB, DataClassesBankDataContext bankDB, DataClassesATMDataContext atmDB)
        {
            bool ok;
            while (true)
            {
                Console.WriteLine("Jaką kwotę chcesz wpłacić?");
                ok = Int32.TryParse(Console.ReadLine(), out int money);

                if (!ok || money < 0 || money*1.0/10 != (int)(money * 1.0 / 10))
                {
                    Console.WriteLine();
                    Console.WriteLine("Podałeś niedozwolony znak! Podaj kwotę całkowitą.");
                    continue;
                }

                Console.WriteLine("Ile banknotów chcesz wpłacić?");
                ok = Int32.TryParse(Console.ReadLine(), out int moneyCount);

                if (!ok || moneyCount < 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("Podałeś niedozwolony znak! Podaj ilość banknotów.");
                    continue;
                }

                var anteBox = atmDB.Money.Single(u => u.Denomination == -1);

                if (anteBox.HowMany < moneyCount)
                {
                    Console.WriteLine();
                    Console.WriteLine("Brak odpowiedniej ilości miejsca we wpłatomoacie!");
                    return false;
                }

                Console.WriteLine("Włóż pieniądze (naciśnij cokolwiek)");

                Console.ReadKey();

                if (!AccountOperationsClass.AnteAccount(Session, userDB, null, bankDB, money))
                {
                    Console.WriteLine("Błąd wpłacania pieniędzy!");
                    Console.WriteLine("~Odbierz pieniądze z powrotem!~");
                    Console.ReadKey();
                }
                else
                {
                    anteBox.HowMany = anteBox.HowMany - moneyCount;
                    atmDB.SubmitChanges();
                }


                break;

            }

            return true;
        }

        static bool EnoughMoneyATM(DataClassesATMDataContext atmDB, int money) //TODO!
        {
            if ((money * 1.0) / 10 != (int)((money * 1.0) / 10) || money == 10 || money == 30)
                return false;

            return true;
        }

        static public bool RechargePhone(string Session, account userDB, DataClassesBankDataContext bankDB)
        {
            bool ok;

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Jaki numer telefonu chcesz doładować? Doładować można tylko PrePaid polskich operatorów! Nie podawaj (+48).");

                ok = Int32.TryParse(Console.ReadLine(), out int number);

                if (ok != true || number > 999999999 || number < 100000000)
                {
                    Console.WriteLine("Podałeś błędny numer telefonu!");
                    continue;
                }
                break;
            }

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Kwota doładowania");

                ok = Int32.TryParse(Console.ReadLine(), out int money);

                if (!ok || money < 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("Podałeś niedozwolony znak! Podaj kwotę całkowitą.");
                    continue;
                }

                if (GetAccountBalance(Session, userDB) < money)
                {
                    Console.WriteLine();
                    Console.WriteLine("Brak odpowiedniej ilości pieniędzy na koncie!");
                    continue;
                }

                if (!AccountOperationsClass.DebitAccount(Session, userDB, bankDB, money))
                {
                    Console.WriteLine();
                    Console.WriteLine("Błąd! Środki nie zostały pobrane.");
                    return false;
                }


                break;

            }

            Console.WriteLine("~Przesyłanie rządania do operatora~");

            return true;
        }

        static public bool TransferMoney(string Session, account userDB, DataClassesBankDataContext bankDB)
        {
            if (userDB.TransferCard)
            {
                bool ok;
                account PayTo;
                while (true)
                {
                    Console.WriteLine("Podaj numer konta na jaki ma zostac wykonany przelew. (Podaj ID karty oosby).");
                    ok = int.TryParse(Console.ReadLine(), out int idCardTo);


                    try
                    {
                        PayTo = bankDB.account.Single(u => u.Id == idCardTo);
                        break;
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Niepoprawny numer konta!");
                        Console.WriteLine();
                    }

                }
                int money;
                while (true)
                {
                    Console.WriteLine("Podaj kwotę jaką chcesz przelać");
                    ok = int.TryParse(Console.ReadLine(), out money);

                    if (!ok || money < 0)
                    {
                        Console.WriteLine();
                        Console.WriteLine("Podałeś niedozwolony znak! Podaj kwotę całkowitą!");
                        continue;
                    }

                    if (GetAccountBalance(Session, userDB) < money)
                    {
                        Console.WriteLine();
                        Console.WriteLine("Brak odpowiedniej ilości pieniędzy na koncie!");
                        continue;
                    }
                    break;
                }
                if (!AccountOperationsClass.DebitAccount(Session, userDB, bankDB, money))
                {
                    Console.WriteLine();
                    Console.WriteLine("Błąd! Środki nie zostały pobrane.");
                    return false;
                }
                else
                {
                    if (!AccountOperationsClass.AnteAccount(Session, userDB, PayTo, bankDB, money))
                    {
                        Console.WriteLine("Błąd! Skontakuj się z bankiem");
                        return false;
                    }
                    Console.WriteLine("~Przelew wykonany pomyślnie!~");
                }



                return true;
            }


            return false;
        }
    }
}
