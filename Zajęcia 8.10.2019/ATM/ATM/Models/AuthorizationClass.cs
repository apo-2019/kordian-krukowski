﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM.Models
{
    class AuthorizationClass
    {
        static public account LoadCard(AccountBankClass user, account userDB, DataClassesBankDataContext bankDB)
        {
            while (true)
            {
                Console.WriteLine("Włóż kartę (wpisz ID karty)");


                try
                {
                    user.Id = Int32.Parse(Console.ReadLine());
                    userDB = bankDB.account.Single(u => u.Id == user.Id);
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Karta uszkodzona (błędne ID karty)");
                    Console.WriteLine();
                }
            }

            return userDB;
        }

        static public string CheckPIN(account userDB, DataClassesBankDataContext bankDB)
        {
            long ses;
            if (CheckLockCartStatus(userDB))
            {
                Console.WriteLine("Karta jest zablokowana - konfiskacja!");
                return null;
            }
            int pin;
            bool result;
            while (true)
            {
                Console.WriteLine("Podaj kod PIN");
                result = Int32.TryParse(Console.ReadLine(), out pin);

                if (result == false || userDB.Pin != pin)
                {
                    Console.WriteLine("Zły PIN!");

                    if (userDB.WrongLeft > 1)
                        userDB.WrongLeft = userDB.WrongLeft - 1;
                    else
                    {
                        Console.WriteLine("Karta została zablokowana!");
                        userDB.WrongLeft = 0;
                        userDB.LockCard = false;
                        return null;
                    }
                    bankDB.SubmitChanges();
                }
                else
                {
                    Console.WriteLine("PIN OK");
                    Random rnd = new Random();
                    ses = rnd.Next(100000000, 900000000);
                    userDB.WrongLeft = 3;
                    userDB.Session = ses.ToString();
                    bankDB.SubmitChanges();
                    break;
                }
            }

            return ses.ToString();
        }

        static public bool CheckSession(string session, account userDB)
        {
            if (userDB.Session == session)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Zwraca "Prawdę" jeśli karta JEST zablokowana!
        /// </summary>
        /// <param name="session"></param>
        /// <param name="userDB"></param>
        /// <returns></returns>
        static public bool CheckLockCartStatus(account userDB)
        {
            if (userDB.LockCard)
                return true;
            else
                return false;
        }
    }
}
