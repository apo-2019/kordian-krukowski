﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM.Models
{
    class AccountBankClass
    {
        public int Id { get; set; }
        public int Pin { get; set; }
        public string Seassion { get; set; }
        public double Money { get; set; }
        public bool LockCard { get; set; }
        public int WrongLeft { get; set; }
    }
}
