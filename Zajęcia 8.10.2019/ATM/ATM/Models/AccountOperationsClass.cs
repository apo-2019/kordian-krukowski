﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM.Models
{
    class AccountOperationsClass
    {
        static public double GetAccountBalance(string Session, account userDB)
        {
            if (!AuthorizationClass.CheckSession(Session, userDB))
                return -1000000000;

            return Double.Parse(userDB.Money.ToString());
        }

        static public bool DebitAccount(string Session, account userDB, DataClassesBankDataContext bankDB, int money)
        {
            if (!AuthorizationClass.CheckSession(Session, userDB))
                return false;

            if (AuthorizationClass.CheckLockCartStatus(userDB) == true)
                return false;

            if (money < 0)
                return false;

            if (GetAccountBalance(Session, userDB) < money)
                return false;

            userDB.Money = userDB.Money - money;
            bankDB.SubmitChanges();

            return true;
        }

        static public bool AnteAccount(string Session, account userDB, account payTo, DataClassesBankDataContext bankDB, int money)
        {
            //if (!AuthorizationClass.CheckSession(Session, userDB))
            //    return false;

            //if (AuthorizationClass.CheckLockCartStatus(userDB) == true)
            //    return false;

            if (money < 0)
                return false;

            if (payTo == null)
            {
                userDB.Money = userDB.Money + money;
            }
            else
            {
                payTo.Money = payTo.Money + money;
            }

            bankDB.SubmitChanges();

            return true;
        }

    }
}
