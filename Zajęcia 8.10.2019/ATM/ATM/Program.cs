﻿using ATM.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM
{
    class Program
    {


        static void Main(string[] args)
        {
            while (true)
            {
                SqlConnection bankDBcon = new SqlConnection(ATM.Properties.Settings.Default.BankDBConnectionString);
                DataClassesBankDataContext bankDB = new DataClassesBankDataContext(bankDBcon);
                SqlConnection atmDBcon = new SqlConnection(ATM.Properties.Settings.Default.AtmDBConnectionString);
                DataClassesATMDataContext atmDB = new DataClassesATMDataContext(atmDBcon);

                AccountBankClass user = new AccountBankClass();
                account userDB = new account();

                string session;
                double temp;


                userDB = AuthorizationClass.LoadCard(user, userDB, bankDB);
                session = AuthorizationClass.CheckPIN(userDB, bankDB);

                if (session == null)
                {
                    Console.ReadKey();
                    System.Threading.Thread.Sleep(4000);
                    Console.Clear();
                    continue;
                }

                string menu;

                while (true)
                {
                    Console.WriteLine("");
                    Console.WriteLine("Wybierz opcję:");
                    Console.WriteLine("1 WYPŁATA");
                    Console.WriteLine("2 WPŁATA");
                    Console.WriteLine("3 DOŁADUJ TELEFON");
                    Console.WriteLine("4 STAN KONTA");
                    Console.WriteLine("5 PRZELEW");
                    Console.WriteLine("6 ~Zakończ~");
                    Console.WriteLine();

                    menu = Console.ReadLine();

                    if (menu == "1")
                    {
                        if (AtmOperationsClass.Withdrow(session, userDB, bankDB, atmDB))
                        {
                            Console.WriteLine("~Odbierz kartę!~");
                            Console.ReadKey();
                            Console.WriteLine("~Odbierz pieniądze!~");
                            Console.ReadKey();
                            session = null;
                            Console.WriteLine("Zapraszamy ponownie!");
                            System.Threading.Thread.Sleep(4000);
                            Console.Clear();
                            break;
                        }
                    }
                    else if (menu == "2")
                    {
                        if (AtmOperationsClass.Ante(session, userDB, bankDB, atmDB))
                        {
                            Console.WriteLine("Pieniądze wpłacone pomyślnie!");
                            Console.WriteLine("~Odbierz kartę!~");
                            session = null;
                            Console.ReadKey();
                            Console.WriteLine("Zapraszamy ponownie!");
                            System.Threading.Thread.Sleep(5000);
                            Console.Clear();
                            break;
                        }
                    }
                    else if (menu == "3")
                    {
                        if (AtmOperationsClass.RechargePhone(session, userDB, bankDB))
                        {
                            Console.WriteLine("Konto zostało doładowane!");
                            Console.WriteLine("~Odbierz kartę!~");
                            session = null;
                            Console.ReadKey();
                            Console.WriteLine("Zapraszamy ponownie!");
                            System.Threading.Thread.Sleep(5000);
                            Console.Clear();
                            break;
                        }
                    }
                    else if (menu == "4")
                    {
                        temp = AtmOperationsClass.GetAccountBalance(session, userDB);

                        if (temp != -1000000000)
                        {
                            Console.WriteLine(temp + " zł");
                        }
                        else
                        {
                            Console.WriteLine("Sesja została przerwana!");
                            break;
                        }
                    }
                    else if (menu == "5")
                    {
                        if  (!AtmOperationsClass.TransferMoney(session, userDB, bankDB))
                        {
                            Console.WriteLine("Opcja nie została aktywowana w banku!");
                        }
                    }
                    else if (menu == "6")
                    {
                        Console.WriteLine("~Odbierz kartę!~");
                        session = null;
                        Console.ReadKey();
                        Console.WriteLine("Zapraszamy ponownie!");
                        System.Threading.Thread.Sleep(4000);
                        Console.Clear();
                        break;
                    }
                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine("~Ten przycisk nic nie robi~");
                        Console.WriteLine();
                    }
                }


                Console.ReadKey();
            }
        }
    }
}
